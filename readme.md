**1. Метод об'єкту** це вбудована в об'єкт функція.

**2. Значення властивостей** об'єкта можуть мати примітивні типи даних (number,
string, boolean, null) або містити в собі інший об'єкт.

**3. Посилальний тип даних** означає, що наприклад при копіюванні
об'єкту ми копіюємо лише посилання на нього, а його вміст залишиться спільним. Якщо ми змінимо якесь значення в одному з
об'єктів, то воно автоматично зміниться і в іншому.